var myJsonDataWisataKuliner = [{
	"id": "1",
	"longitude": "110.390140",
	"latitude": "-7.805686",
	"address": "Jl. Janturan UH/IV No. 36, Warungboto, Umbulharjo, Warungboto, Umbulharjo, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55164",
	"description": "",
	"altitude": "100.0",
	"name": "Gudeg Pawon"
},{
	"id": "2",
	"longitude": "110.368970",
	"latitude": "-7.800722",
	"address": "Jl. Mayor Suryotomo No.7, Ngupasan, Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122",
	"description": "",
	"altitude": "100.0",
	"name": "Nasi Goreng Beringharjo"
},{
	"id": "3",
	"longitude": "110.377396",
	"latitude": "-7.781305",
	"address": "Jl. Sagan No.4, Terban, Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55223",
	"description": "",
	"altitude": "100.0",
	"name": "Bong Kopitown"
},  {
	"id": "4",
	"longitude": "110.371495",
	"latitude": "-7.785149",
	"address": "Jalan Faridan Muridan Noto No.7, Kotabaru, Gondokusuman, Kotabaru, Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55224",
	"description": "",
	"altitude": "100.0",
	"name": "The House of Raminten"
},{
	"id": "5",
	"longitude": "110.357795",
	"latitude": "-7.801101",
	"address": "Jalan KH. Ahmad Dahlan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262",
	"description": "",
	"altitude": "100.0",
	"name": "Oseng-Oseng Mercon Bu Narti"
},{
	"id": "6",
	"longitude": "110.395348",
	"latitude": "-7.775737",
	"address": "Jl. Wulung Lor, Papringan, Caturtunggal, Depok, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281",
	"description": "",
	"altitude": "100.0",
	"name": "Warung Makan Ayam Geprek Bu Rum 1"
}, {
	"id": "7",
	"longitude": "110.373818",
	"latitude": "-7.782879",
	"address": "Jl. Jenderal Sudirman, Terban, Gondokusuman, Terban, Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55224",
	"description": "",
	"altitude": "100.0",
	"name": "Soto Ayam Kampung Pak Dalbe"
},{
	"id": "8",
	"longitude": "110.366857",
	"latitude": "-7.781616",
	"address": "Jl. Kranggan No.2, Cokrodiningratan, Jetis, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55233",
	"description": "",
	"altitude": "100.0",
	"name": "Soto Sampah"
}, {
	"id": "9",
	"longitude": "110.361201",
	"latitude": "-7.705405",
	"address": "Jl. Pramuka No. 53, Pandowoharjo, Sleman, Pandowoharjo, Kec. Sleman, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55512",
	"description": "",
	"altitude": "100.0",
	"name": "Jejamuran"
},{
	"id": "10",
	"longitude": "110.366756",
	"latitude": "-7.804598",
	"address": "Jalan Wijilan No. 167, Kraton, Panembahan, Kraton, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55131",
	"description": "",
	"altitude": "100.0",
	"name": "Gudeg Yu Djum Wijilan 167"
}, {
	"id": "11",
	"longitude": "110.387471",
	"latitude": "-7.871257",
	"address": "Jalan Imogiri Timur Km. 10, Wonokromo, Pleret, Wonokromo, Pleret, Bantul, Daerah Istimewa Yogyakarta 55791",
	"description": "",
	"altitude": "100.0",
	"name": "Sate Klathak Pak Pong"
},{
	"id": "12",
	"longitude": "110.376419",
	"latitude": "-7.755042",
	"address": "Pogung Lor No.263, Sinduadi, Mlati, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55284",
	"description": "",
	"altitude": "100.0",
	"name": "Angkringan Lik Man"
},{
	"id": "13",
	"longitude": "110.353369",
	"latitude": "-7.815631",
	"address": "Gedongkiwo, Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55142",
	"description": "",
	"altitude": "100.0",
	"name": "Mie Ayam Grabyas"
}];