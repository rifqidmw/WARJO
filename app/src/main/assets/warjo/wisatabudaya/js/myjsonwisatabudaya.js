var myJsonDataWisataBudaya = [{
	"id": "1",
	"longitude": "110.447228",
	"latitude": "-7.762472",
	"address": "Jl. Candi Sambisari, Purwomartani, Kalasan, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55571",
	"description": "",
	"altitude": "100.0",
	"name": "Candi Sambisari"
},{
	"id": "2",
	"longitude": "110.367065",
	"latitude": "-7.782879",
	"address": "Jalan Jendral Sudirman, Gowongan, Jetis, Gowongan, Jetis, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55233",
	"description": "",
	"altitude": "100.0",
	"name": "Tugu Jogja"
},{
	"id": "3",
	"longitude": "110.494072",
	"latitude": "-7.777964",
	"address": "Dusun Cepit, Desa Bokoharjo, Prambanan, Sleman, Bokoharjo, Prambanan, Jogjakarta, Daerah Istimewa Yogyakarta 55572",
	"description": "",
	"altitude": "100.0",
	"name": "Candi Banyunibo"
},  {
	"id": "4",
	"longitude": "110.474252",
	"latitude": "-7.761524",
	"address": "Bendan, Tirtomartani, Kalasan, Tirtomartani, Kalasan, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55571",
	"description": "",
	"altitude": "100.0",
	"name": "Candi Sari"
},{
	"id": "5",
	"longitude": "110.491467",
	"latitude": "-7.752010",
	"address": "Bokoharjo, Kec. Prambanan, Kabupaten Sleman, Daerah Istimewa Yogyakarta",
	"description": "",
	"altitude": "100.0",
	"name": "Candi Prambanan"
},{
	"id": "6",
	"longitude": "110.457559",
	"latitude": "-7.833499",
	"address": "Jl. Wonosari, Srimulyo, Piyungan, Bantul, Daerah Istimewa Yogyakarta 55792",
	"description": "",
	"altitude": "100.0",
	"name": "Situs Payak"
}, {
	"id": "7",
	"longitude": "110.364203",
	"latitude": "-7.805284",
	"address": "Jalan Rotowijayan Blok No. 1, Panembahan, Kraton, Kota Yogyakarta, Daerah Istimewa Yogyakarta",
	"description": "",
	"altitude": "100.0",
	"name": "Keraton Yogyakarta"
},{
	"id": "8",
	"longitude": "110.472350",
	"latitude": "-7.767295",
	"address": "Jl. Raya Yogya - Solo, Suryatmajan, Danurejan, Daerah Istimewa Yogyakarta",
	"description": "",
	"altitude": "100.0",
	"name": "Candi Kalasan"
}, {
	"id": "9",
	"longitude": "110.417623",
	"latitude": "-7.815679",
	"address": "Jl. Wonosari, Baturetno, Banguntapan, Bantul, Daerah Istimewa Yogyakarta 55197",
	"description": "",
	"altitude": "100.0",
	"name": "Candi Mantup"
},{
	"id": "10",
	"longitude": "110.366311",
	"latitude": "-7.800250",
	"address": "Jl. Margo Mulyo No.6, Ngupasan, Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122",
	"description": "",
	"altitude": "100.0",
	"name": "Museum Benteng Vredeburg"
},
{
	"id": "11",
	"longitude": "110.468675",
	"latitude": "-7.810383",
	"address": "Jogotirto, Berbah, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55573",
	"description": "",
	"altitude": "100.0",
	"name": "Candi Abang"
},{
	"id": "12",
	"longitude": "110.364451",
	"latitude": "-7.801960",
	"address": "Jalan Pangurakan No. 6, Ngupasan, Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122",
	"description": "",
	"altitude": "100.0",
	"name": "Museum Sonobudoyo"
},{
	"id": "13",
	"longitude": "110.415679",
	"latitude": "-7.789934",
	"address": "Kompleks Landasan Udara Adisucipto, Jl. Kolonel Sugiono, Banguntapan, Yogyakarta, Bantul, Daerah Istimewa Yogyakarta 55282",
	"description": "",
	"altitude": "100.0",
	"name": "Museum Pusat TNI AU Dirgantara Mandala"
},  {
	"id": "14",
	"longitude": "110.359007",
	"latitude": "-7.810039",
	"address": "Komplek Wisata Taman Sari, Taman, Patehan, Kraton, Patehan, Kraton, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55133",
	"description": "",
	"altitude": "100.0",
	"name": "Kampung Wisata Taman Sari"
},{
	"id": "15",
	"longitude": "110.426219",
	"latitude": "-7.812768",
	"address": "Sendangtirto, Berbah, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55573",
	"description": "",
	"altitude": "100.0",
	"name": "Candi Klodangan"
},{
	"id": "16",
	"longitude": "110.396397",
	"latitude": "-7.782745",
	"address": "Jl. Laksda Adisucipto No.167, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281",
	"description": "",
	"altitude": "100.0",
	"name": "Affandi Museum"
}, {
	"id": "17",
	"longitude": "110.393232",
	"latitude": "-7.810297",
	"address": "Jl. Veteran No.77, Warungboto, Umbulharjo, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55164",
	"description": "",
	"altitude": "100.0",
	"name": "Situs Warungboto"
},{
	"id": "18",
	"longitude": "110.3913003",
	"latitude": "-7.791605",
	"address": "Jl. Jend. Sudirman No.75, Terban, Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55223",
	"description": "",
	"altitude": "100.0",
	"name": "Museum Pusat TNI AD Dharma Wiratama"
}, {
	"id": "19",
	"longitude": "110.365285",
	"latitude": "-7.800988",
	"address": "Jalan Ahmad Yani, Ngupasan, Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122",
	"description": "",
	"altitude": "100.0",
	"name": "Monumen Serangan Umum 1 Maret"
},{
	"id": "20",
	"longitude": "110.369596",
	"latitude": "-7.749622",
	"address": "Jl. Ringroad Utara No.15, RW.21, Sariharjo, Ngaglik, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55581",
	"description": "",
	"altitude": "100.0",
	"name": "Monumen Yogya Kembali"
}];