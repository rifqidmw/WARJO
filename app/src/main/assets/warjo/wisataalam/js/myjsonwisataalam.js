var myJsonDataWisataAlam = [{
	"id": "1",
	"longitude": "110.437584",
	"latitude": "-7.901377",
	"address": "Gunungcilik RT. 07 / RW. 02, Muntuk, Dlingo, Muntuk, Dlingo, Bantul, Daerah Istimewa Yogyakarta 55783",
	"description": "www.puncakbecici.blogspot.com",
	"altitude": "100.0",
	"name": "Puncak Becici"
},  {
	"id": "2",
	"longitude": "110.613231",
	"latitude": "-8.151213",
	"address": "Tepus, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta",
	"description": "eksotisjogja.com/pantai-indrayanti-gunung-kidul/",
	"altitude": "100.0",
	"name": "Pantai Indrayanti"
},{
	"id": "3",
	"longitude": "110.683218",
	"latitude": "-8.181803",
	"address": "Purwodadi, Tepus, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55881",
	"description": "eksotisjogja.com/pantai-timang-gunung-kidul/",
	"altitude": "100.0",
	"name": "Pantai Siung"
},{
	"id": "4",
	"longitude": "110.679229",
	"latitude": "-8.182735",
	"address": "Purwodadi, Tepus, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta",
	"description": "eksotisjogja.com/pantai-nglambor-gunung-kidul/",
	"altitude": "100.0",
	"name": "Pantai Ngelambor"
}, {
	"id": "5",
	"longitude": "110.700290",
	"latitude": "-8.186053",
	"address": "Balong, Girisubo, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55883",
	"description": "eksotisjogja.com/pantai-watu-lumbung/",
	"altitude": "100.0",
	"name": "Pantai Watu Lumbung"
},{
	"id": "6",
	"longitude": "110.607912",
	"latitude": "-8.147042",
	"address": "Tepus, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55881",
	"description": "eksotisjogja.com/pantai-sundak-gunung-kidul/",
	"altitude": "100.0",
	"name": "Pantai Sundak"
}, {
	"id": "7",
	"longitude": "110.480242",
	"latitude": "-7.845668",
	"address": "Patuk, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55792",
	"description": "eksotisjogja.com/bukit-bintang-jogja/",
	"altitude": "100.0",
	"name": "Bukit Bintang"
},{
	"id": "8",
	"longitude": "110.450260",
	"latitude": "-7.704425",
	"address": "Widodomartani, Ngemplak, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55584",
	"description": "eksotisjogja.com/blue-lagoon-yempat-pemandian-tirta-budi/",
	"altitude": "100.0",
	"name": "Blue Lagoon Jogja"
}, {
	"id": "9",
	"longitude": "110.430518",
	"latitude": "-7.594286",
	"address": "Jl. Tlogo Putri, Hargobinangun, Pakem, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55582",
	"description": "eksotisjogja.com/tlogo-putri-kaliurang-wisata-alam-lereng-gunung-merapi/",
	"altitude": "100.0",
	"name": "Tlogo Putri Kaliurang"
},{
	"id": "10",
	"longitude": "110.504760",
	"latitude": "-7.782343",
	"address": "Sambirejo, Prambanan, Sambirejo, Prambanan, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55572",
	"description": "eksotisjogja.com/objek-wisata-taman-tebing-breksi-yogyakarta/",
	"altitude": "100.0",
	"name": "Taman Tebing Breksi"
},{
	"id": "11",
	"longitude": "110.639005",
	"latitude": "-8.009954",
	"address": "Jetisa Wetan, Pacarejo, Semanu, Pacarejo, Semanu, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55893",
	"description": "eksotisjogja.com/serunya-berpetualang-cave-tubing-kalisuci-gunungkidul/",
	"altitude": "100.0",
	"name": "Cave Tubing Kalisuci"
},{
	"id": "12",
	"longitude": "110.652634",
	"latitude": "-8.170343",
	"address": "Sureng, Purwodadi, Tepus, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55881",
	"description": "eksotisjogja.com/pantai-ngetun-gunungkidul/",
	"altitude": "100.0",
	"name": "Pantai Ngetun"
},{
	"id": "13",
	"longitude": "110.534224",
	"latitude": "-8.129550",
	"address": "Kanigoro, Tanjungsari, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55871",
	"description": "eksotisjogja.com/pantai-kayu-arum-gunungkidul/",
	"altitude": "100.0",
	"name": "Pantai Kayu Arum"
},  {
	"id": "14",
	"longitude": "110.638397",
	"latitude": "-8.028671",
	"address": "Pacarejo, Semanu, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55893",
	"description": "eksotisjogja.com/goa-jomblang-gunungkidul/",
	"altitude": "100.0",
	"name": "Goa Jomblang"
},{
	"id": "15",
	"longitude": "110.615571",
	"latitude": "-7.813083",
	"address": "Mertelu, Gedang Sari, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55863",
	"description": "eksotisjogja.com/goa-jomblang-gunungkidul/",
	"altitude": "100.0",
	"name": "Green Village Gedangsari"
},{
	"id": "16",
	"longitude": "110.549547",
	"latitude": "-7.843063",
	"address": "Nglanggeran, Patuk, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55862",
	"description": "eksotisjogja.com/gunung-api-purba-nglanggeran/",
	"altitude": "100.0",
	"name": "Gunung Api Purba Nglanggeran"
}, {
	"id": "17",
	"longitude": "110.633017",
	"latitude": "-7.832079",
	"address": "Pilangrejo, Nglipar, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55852",
	"description": "eksotisjogja.com/embung-batara-sriten-gunung-kidul/",
	"altitude": "100.0",
	"name": "Embung Batara Srinten"
},{
	"id": "18",
	"longitude": "110.572211",
	"latitude": "-7.809848",
	"address": "Jl. Juminahan, Sampang, Gedang Sari, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55863",
	"description": "eksotisjogja.com/embung-batara-sriten-gunung-kidul/",
	"altitude": "100.0",
	"name": "Air Terjun Luweng Sampang"
}, {
	"id": "19",
	"longitude": "110.799017",
	"latitude": "-8.188879",
	"address": "Songbanyu, Girisubo, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55883",
	"description": "eksotisjogja.com/pantai-sadeng-gunung-kidul/",
	"altitude": "100.0",
	"name": "Pantai Sadeng"
},{
	"id": "20",
	"longitude": "110.514192",
	"latitude": "-8.121085",
	"address": "Wonosari, Kanigoro, Sapto Sari, Gunung Kidul Regency, Special Region of Yogyakarta 55871",
	"description": "eksotisjogja.com/pantai-ngrenehan-gunungkidul/",
	"altitude": "100.0",
	"name": "Pantai Ngrenehan"
}, {
	"id": "21",
	"longitude": "110.546883",
	"latitude": "-7.846992",
	"address": "Nglanggeran, Patuk, Nglanggeran, Patuk, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55862",
	"description": "eksotisjogja.com/embung-nglanggeran-gunung-kidul/",
	"altitude": "100.0",
	"name": "Embung Nglanggeran"
}];