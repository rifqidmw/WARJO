// implementation of AR-Experience (aka "World")
	//window.hasil=myJsonData;

//	$('#kampus').on('change', function(){
//		var kampus= $(this).val();
//
//		if(kampus=="semua"){
//			window.hasil=myJsonData;
//		}
//		else if(kampus=="it"){
//			window.hasil=myJsonData2;
//		}
//	});

// holds server information
var World = {
	// you may request new data from server periodically, however: in this sample data is only requested once
	isRequestingData: false,

	// true once data was fetched
	initiallyLoadedData: false,

	// different POI-Marker assets
	markerDrawable_idle: null,
	markerDrawable_selected: null,
	markerDrawable_directionIndicator: null,

	// list of AR.GeoObjects that are currently shown in the scene / World
	markerList: [],
	markerListUnclustered: [],
	// this is the container for the Clusters
	placeGeoObjects: [],

	// The last selected marker
	currentMarker: null,
	
	locationUpdateCounter: 0,
	updatePlacemarkDistancesEveryXLocationUpdates: 10,


	// called to inject new POI data
	loadPoisFromJsonData: function loadPoisFromJsonDataFn(poiData) {
		
		// destroys all existing AR-Objects (markers & radar)
		AR.context.destroyAll();

		// show radar & set click-listener
		PoiRadar.show();
		$('#radarContainer').unbind('click');
		$("#radarContainer").click(PoiRadar.clickedRadar);

		// empty list of visible markers
		World.markerList = [];

		// start loading marker assets
		World.markerDrawable_idle = new AR.ImageResource("assets/marker_idle.png");
		World.markerDrawable_selected = new AR.ImageResource("assets/marker_selected.png");
		World.markerDrawable_directionIndicator = new AR.ImageResource("assets/indi.png");

		// loop through POI-information and create an AR.GeoObject (=Marker) per POI
		for (var currentPlaceNr = 0; currentPlaceNr < poiData.length; currentPlaceNr++) {
			var singlePoi = {
				"id": poiData[currentPlaceNr].id,
				"latitude": parseFloat(poiData[currentPlaceNr].latitude),
				"longitude": parseFloat(poiData[currentPlaceNr].longitude),
				"altitude": parseFloat(poiData[currentPlaceNr].altitude),
				"title": poiData[currentPlaceNr].name,
				"description": poiData[currentPlaceNr].description,
				"img": poiData[currentPlaceNr].img,
				"img1": poiData[currentPlaceNr].img1,
				"img2": poiData[currentPlaceNr].img2,
				"img3": poiData[currentPlaceNr].img3,
				"img4": poiData[currentPlaceNr].img4,
				"img5": poiData[currentPlaceNr].img5
			};

			//World.markerList.push(new Marker(singlePoi));
		   //here just the POI list is being created out of the JSON file
			World.markerListUnclustered.push(singlePoi);
		}
		
		//this is the magic Call that clusters the pois
		//the first parameter 20 is for the angle in which pois will be clustered

		World.placeGeoObjects  = ClusterHelper.createClusteredPlaces(20, World.userLocation, World.markerListUnclustered);

          //go through all clusters
          for (var i=0; i<World.placeGeoObjects.length; i++) {
           //go through all items in each cluster
           for (var j=0; j<World.placeGeoObjects[i].places.length; j++) {

           		singlePoi = World.placeGeoObjects[i].places[j];
           		// the singlePoi altitude is originally 0
           		// it will be increased by 250 for each item remaining in the cluster
           		singlePoi.altitude =  j * 250;
				// Add pois to World
           		World.markerList.push(new Marker(singlePoi));
           }
          }
		// updates distance information of all placemarks
		World.updateDistanceToUserValues();

		World.updateStatusMessage(currentPlaceNr + ' places loaded');
		
		
			// set distance slider to 100%
		$("#panel-distance-range").val(100);
		$("#panel-distance-range").slider("refresh");
	},

	// sets/updates distances of all makers so they are available way faster than calling (time-consuming) distanceToUser() method all the time
	updateDistanceToUserValues: function updateDistanceToUserValuesFn() {
		for (var i = 0; i < World.markerList.length; i++) {
			World.markerList[i].distanceToUser = World.markerList[i].markerObject.locations[0].distanceToUser();

			var distanceToUserValue = (World.markerList[i].distanceToUser > 999) ? ((World.markerList[i].distanceToUser / 1000).toFixed(2) + " km") : (Math.round(World.markerList[i].distanceToUser) + " m");
			World.markerList[i].descriptionLabel.text = distanceToUserValue;
		}
	},
	
	// updates status message shon in small "i"-button aligned bottom center
	updateStatusMessage: function updateStatusMessageFn(message, isWarning) {

		var themeToUse = isWarning ? "e" : "c";
		var iconToUse = isWarning ? "alert" : "info";

		$("#status-message").html(message);
		$("#popupInfoButton").buttonMarkup({
			theme: themeToUse
		});
		$("#popupInfoButton").buttonMarkup({
			icon: iconToUse
		});
	},
	
	/*
		It may make sense to display POI details in your native style.
		In this sample a very simple native screen opens when user presses the 'More' button in HTML.
		This demoes the interaction between JavaScript and native code.
	*/
	// user clicked "More" button in POI-detail panel -> fire event to open native screen
	onPoiDetailMoreButtonClicked: function onPoiDetailMoreButtonClickedFn() {
		var currentMarker = World.currentMarker;
		var architectSdkUrl =
			"architectsdk://markerselected?id=" + encodeURIComponent(currentMarker.poiData.id) +
			"&latitude=" + encodeURIComponent(currentMarker.poiData.latitude) +
			"&longitude=" + encodeURIComponent(currentMarker.poiData.longitude) +
			"&title=" + encodeURIComponent(currentMarker.poiData.title) +
			"&address=" + encodeURIComponent(currentMarker.poiData.address) +
			"&phone=" + encodeURIComponent(currentMarker.poiData.phone) +
			"&description=" + encodeURIComponent(currentMarker.poiData.description) +
			"&img=" + encodeURIComponent(currentMarker.poiData.img)+
			"&img1=" + encodeURIComponent(currentMarker.poiData.img1) +
			"&img2=" + encodeURIComponent(currentMarker.poiData.img2) +
			"&img3=" + encodeURIComponent(currentMarker.poiData.img3) +
			"&img4=" + encodeURIComponent(currentMarker.poiData.img4) +
			"&img5=" + encodeURIComponent(currentMarker.poiData.img5);

		/*
			The urlListener of the native project intercepts this call and parses the arguments.
			This is the only way to pass information from JavaSCript to your native code.
			Ensure to properly encode and decode arguments.
			Note: you must use 'document.location = "architectsdk://...' to pass information from JavaScript to native.
			! This will cause an HTTP error if you didn't register a urlListener in native architectView !
		*/
		document.location = architectSdkUrl;
	},
	

	// location updates, fired every time you call architectView.setLocation() in native environment
	locationChanged: function locationChangedFn(lat, lon, alt, acc) {
		
		
		// store user's current location in World.userLocation, so you always know where user is
		World.userLocation = {
			'latitude': lat,
			'longitude': lon,
			'altitude': alt,
			'accuracy': acc
		};

		// request data if not already present
		if (!World.initiallyLoadedData) {
			World.requestDataFromLocal(lat, lon);
			World.initiallyLoadedData = true;
		} else if (World.locationUpdateCounter === 0) {
			// update placemark distance information frequently, you max also update distances only every 10m with some more effort
			World.updateDistanceToUserValues();
		}

		// helper used to update placemark information every now and then (e.g. every 10 location upadtes fired)
		World.locationUpdateCounter = (++World.locationUpdateCounter % World.updatePlacemarkDistancesEveryXLocationUpdates);
	},

	// fired when user pressed maker in cam
	onMarkerSelected: function onMarkerSelectedFn(marker) {
World.currentMarker =marker;

		// update panel values
		$("#poi-detail-title").html(marker.poiData.title);
		$("#poi-detail-address").html(marker.poiData.address);
		//$("#poi-detail-description").html(marker.poiData.description);

		var distanceToUserValue = (marker.distanceToUser > 999) ? ((marker.distanceToUser / 1000).toFixed(2) + " km") : (Math.round(marker.distanceToUser) + " m");

		$("#poi-detail-distance").html(distanceToUserValue);

		// show panel
		$("#panel-poidetail").panel("open", 123);

		$(".ui-panel-dismiss").unbind("mousedown");

		$("#panel-poidetail").on("panelbeforeclose", function(event, ui) {
			World.currentMarker.setDeselected(World.currentMarker);
		});
	},

	// screen was clicked but no geo-object was hit
	onScreenClick: function onScreenClickFn() {
		if (World.currentMarker) {
			World.currentMarker.setDeselected(World.currentMarker);
		}
	},

	/*
		In case the data of your ARchitect World is static the content should be stored within the application. 
		Create a JavaScript file (e.g. myJsonData.js) where a globally accessible variable is defined.
		Include the JavaScript in the ARchitect Worlds HTML by adding <script src="js/myJsonData.js"/> to make POI information available anywhere in your JavaScript.
	*/

	// returns distance in meters of placemark with maxdistance * 1.1
	getMaxDistance: function getMaxDistanceFn() {

		// sort palces by distance so the first entry is the one with the maximum distance
		World.markerList.sort(World.sortByDistanceSortingDescending);

		// use distanceToUser to get max-distance
		var maxDistanceMeters = World.markerList[0].distanceToUser;

		// return maximum distance times some factor >1.0 so ther is some room left and small movements of user don't cause places far away to disappear
		return maxDistanceMeters * 1.1;
	},
	
	// udpates values jarak
	updateRangeValues: function updateRangeValuesFn() {

		// get current slider value (0..100);
		var slider_value = $("#panel-distance-range").val();

		// max range relative to the maximum distance of all visible places
		var maxRangeMeters = Math.round(World.getMaxDistance() * (slider_value / 100));

		// range in meters including metric m/km
		var maxRangeValue = (maxRangeMeters > 999) ? ((maxRangeMeters / 1000).toFixed(2) + " km") : (Math.round(maxRangeMeters) + " m");

		// number of places within max-range
		var placesInRange = World.getNumberOfVisiblePlacesInRange(maxRangeMeters);

		// update UI labels accordingly
		$("#panel-distance-value").html(maxRangeValue);
		//$("#panel-distance-places").html((placesInRange != 1) ? (placesInRange + " Places") : (placesInRange + " Place"));
		$("#panel-distance-places").html((placesInRange + " Lokasi"));

		// update culling distance, so only palces within given range are rendered
		AR.context.scene.cullingDistance = Math.max(maxRangeMeters, 1);

		// update radar's maxDistance so radius of radar is updated too
		PoiRadar.setMaxDistance(Math.max(maxRangeMeters, 1));
	},
	
	// returns number of places with same or lower distance than given range
	getNumberOfVisiblePlacesInRange: function getNumberOfVisiblePlacesInRangeFn(maxRangeMeters) {

		// sort markers by distance
		World.markerList.sort(World.sortByDistanceSorting);

		// loop through list and stop once a placemark is out of range ( -> very basic implementation )
		for (var i = 0; i < World.markerList.length; i++) {
			if (World.markerList[i].distanceToUser > maxRangeMeters) {
				return i;
			}
		};

		// in case no placemark is out of range -> all are visible
		return World.markerList.length;
	},

// udpates values zoom
	updateRangeValuesZoom: function updateRangeValuesZoomFn() {

		// get current slider value (0..100);
		var slider_value_zoom = $("#panel-zoom-range").val();

		// zoom level (1 up to max available)
		var maxRangeValueZoom = (AR.hardware.camera.features.zoomRange.max - 1) * (slider_value_zoom / 100) + 1;

		// update UI labels accordingly
		$("#panel-zoom-value").html(maxRangeValueZoom);

		AR.hardware.camera.zoom = maxRangeValueZoom;
	},

	handlePanelMovements: function handlePanelMovementsFn() {

		$("#panel-pengaturan").on("panelclose", function(event, ui) {
			$("#radarContainer").addClass("radarContainer_left");
			$("#radarContainer").removeClass("radarContainer_right");
			PoiRadar.updatePosition();
		});

		$("#panel-pengaturan").on("panelopen", function(event, ui) {
			$("#radarContainer").removeClass("radarContainer_left");
			$("#radarContainer").addClass("radarContainer_right");
			PoiRadar.updatePosition();
		});
	},

    // pengaturan
	pengaturanMarkerKamera: function pengaturanMarkerKameraFn() {
		if (World.markerList.length > 0) {

			// update labels on every range movement
			$('#panel-distance-range').change(function() {
				World.updateRangeValues();
			});

			World.updateRangeValues();
			World.handlePanelMovements();

			// update labels on every range movement
            $('#panel-zoom-range').change(function() {
                World.updateRangeValuesZoom();
            });

            World.updateRangeValuesZoom();

            // open panel
            $("#panel-pengaturan").trigger("updatelayout");
            $("#panel-pengaturan").panel("open", 1234);
		} else {

			// no places are visible, because the are not loaded yet
			World.updateStatusMessage('Lokasi angkutan tidak ditemukan.', true);
		}
	},

	// reload places from content source
	reloadPlaces: function reloadPlacesFn() {
		if (!World.isRequestingData) {
			if (World.userLocation) {
				//AR.context.destroyAll();

				//$(".markerList").attr("html","");
				//World.loadPoisFromJsonData();
				//World.markerList("refresh");
				//World();

				World.requestDataFromLocal(World.userLocation.latitude, World.userLocation.longitude);

			} else {
				World.updateStatusMessage('Lokasi anda tidak diketahui.', true);
			}
		}
		else {
			World.updateStatusMessage('Memuat lokasi angkutan...', true);
		}
	},

	// request POI data
	requestDataFromLocal: function requestDataFromLocalFn(lat, lon) {
			World.loadPoisFromJsonData(myJsonDataWisataSejarah);

	},
	
	// helper to sort places by distance
	sortByDistanceSorting: function(a, b) {
		return a.distanceToUser - b.distanceToUser;
	},

	// helper to sort places by distance, descending
	sortByDistanceSortingDescending: function(a, b) {
		return b.distanceToUser - a.distanceToUser;
	}

};




/* forward locationChanges to custom function */
AR.context.onLocationChanged = World.locationChanged;

/* forward clicks in empty area to World */
AR.context.onScreenClick = World.onScreenClick;