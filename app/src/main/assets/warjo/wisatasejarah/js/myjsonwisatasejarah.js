var myJsonDataWisataSejarah = [{
	"id": "1",
	"longitude": "110.3804248",
	"latitude": "-7.8204701",
	"description": "www.stikes-yogyakarta.ac.id",
	"altitude": "100.0",
	"name": "STIKES-Yk"
},{
	"id": "2",
	"longitude": "110.3223853",
	"latitude": "-7.805519",
	"description": "www.stikesayaniyk.ac.id",
	"altitude": "100.0",
	"name": "STIKES-AYani"
},{
	"id": "3",
	"longitude": "110.3423116",
	"latitude": "-7.8265002",
	"description": "www.stikessuryaglobal.ac.id",
	"altitude": "100.0",
	"name": "STIKES-SuryaGlb"
},  {
	"id": "4",
	"longitude": "110.3222234",
	"latitude": "-7.8186409",
	"description": "www.almaata.ac.id",
	"altitude": "100.0",
	"name": "STIKES-Almaata"
},{
	"id": "5",
	"longitude": "110.3736433",
	"latitude": "-7.7841415",
	"description": "www.stikesbethesda.ac.id",
	"altitude": "100.0",
	"name": "STIKES-Bethesda"
},{
	"id": "6",
	"longitude": "110.4069476",
	"latitude": "-7.7606968",
	"description": "www.gunabangsa.ac.id",
	"altitude": "100.0",
	"name": "STIKES-GBangsa"
}, {
	"id": "7",
	"longitude": "110.408285",
	"latitude": "-7.7690912",
	"description": "www.stieykpn.ac.id",
	"altitude": "100.0",
	"name": "STIE-YKPN"
},{
	"id": "8",
	"longitude": "110.3913003",
	"latitude": "-7.791605",
	"description": "www.apmd.ac.id",
	"altitude": "100.0",
	"name": "APMD"
}, {
	"id": "9",
	"longitude": "110.3411335",
	"latitude": "-7.7827274",
	"description": "www.stpn.ac.id",
	"altitude": "100.0",
	"name": "STPN"
},{
	"id": "10",
	"longitude": "110.3662906",
	"latitude": "-7.8381577",
	"description": "www.sttkd.ac.id",
	"altitude": "100.0",
	"name": "STTKD"
}, {
	"id": "11",
	"longitude": "110.406715",
	"latitude": "-7.7967866",
	"description": "www.akakom.ac.id",
	"altitude": "100.0",
	"name": "STMIK-Akakom"
},{
	"id": "12",
	"longitude": "110.4011373",
	"latitude": "-7.781039",
	"description": "www.stipram.ac.id",
	"altitude": "100.0",
	"name": "STIPRAM"
},{
	"id": "13",
	"longitude": "110.4120794",
	"latitude": "-7.7974882",
	"description": "www.stta.ac.id",
	"altitude": "100.0",
	"name": "STTA"
},{
	"id": "14",
	"longitude": "110.4143265",
	"latitude": "-7.7733518",
	"description": "www.sttnas.ac.id",
	"altitude": "100.0",
	"name": "STTNAS"
},{
	"id": "15",
	"longitude": "110.4012747",
	"latitude": "-7.7785195",
	"description": "www.ampta.ac.id",
	"altitude": "100.0",
	"name": "STIPAR-Ampta"
},{
	"id": "49",
	"longitude": "110.3285913",
	"latitude": "-7.793378",
	"description": "www.stmikayani.ac.id",
	"altitude": "100.0",
	"name": "STMIK-AYani"
}];