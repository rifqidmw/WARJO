package elinfo.warjo;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import elinfo.warjo.ListWisataAlam.MainListWisataAlam;
import elinfo.warjo.ListWisataBudaya.MainListWisataBudaya;
import elinfo.warjo.ListWisataKuliner.MainListWisataKuliner;

public class MenuUtama extends AppCompatActivity {
    public ImageButton btn_wisata_alam, btn_wisata_sejarah, btn_wisata_kuliner, btn_wisata,budaya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);

        ImageButton btn_wisata_alam = (ImageButton) findViewById(R.id.btn_wisata_alam);
        btn_wisata_alam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogWisataAlam();
            }
        });
        ImageButton btn_wisata_kuliner = (ImageButton) findViewById(R.id.btn_wisata_kuliner);
        btn_wisata_kuliner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogWisataKuliner();
            }
        });
        ImageButton btn_wisata_budaya = (ImageButton) findViewById(R.id.btn_wisata_budaya);
        btn_wisata_budaya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogWisataBudaya();
            }
        });
    }
    private void showDialogWisataAlam(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Wisata Alam");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Pilih Menu")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("List",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        Intent intent = new Intent (MenuUtama.this, MainListWisataAlam.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("AR",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol ini diklik, akan menutup dialog
                        // dan tidak terjadi apa2
                        Intent intent = new Intent (MenuUtama.this, WisataAlam.class);
                        startActivity(intent);
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }
    private void showDialogWisataBudaya(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Wisata Sejarah & Budaya");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Pilih Menu")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("List",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        Intent intent = new Intent (MenuUtama.this, MainListWisataBudaya.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("AR",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol ini diklik, akan menutup dialog
                        // dan tidak terjadi apa2
                        Intent intent = new Intent (MenuUtama.this, WisataBudaya.class);
                        startActivity(intent);
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }
    private void showDialogWisataKuliner(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title dialog
        alertDialogBuilder.setTitle("Wisata Kuliner");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Pilih Menu")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("List",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        Intent intent = new Intent (MenuUtama.this, MainListWisataKuliner.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("AR",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol ini diklik, akan menutup dialog
                        // dan tidak terjadi apa2
                        Intent intent = new Intent (MenuUtama.this, WisataKuliner.class);
                        startActivity(intent);
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }
}
