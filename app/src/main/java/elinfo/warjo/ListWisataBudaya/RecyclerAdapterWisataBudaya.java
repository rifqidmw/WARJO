package elinfo.warjo.ListWisataBudaya;

/**
 * Created by Sei on 17/11/2017.
 */
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import elinfo.warjo.ListWisataBudaya.RecyclerViewHolderWisataBudaya;
import elinfo.warjo.MenuUtama;
import elinfo.warjo.R;

public class RecyclerAdapterWisataBudaya extends  RecyclerView.Adapter<RecyclerViewHolderWisataBudaya> {

    //deklarasi variable context

    private final Context context;

    String [] name={"Candi Sambisari","Tugu Jogja","Candi Banyunibo",
            "Candi Sari","Candi Prambanan","Situs Payak",
            "Keraton Yogyakarta","Candi Kalasan","Candi Mantup",
            "Museum Benteng Vredeburg","Candi Abang","Museum Sonobudoyo",
            "Museum Pusat TNI AU Dirgantara Mandala","Kampung Wisata Taman Sari",
            "Candi Klodangan","Affandi Museum","Situs Warungboto",
            "Museum Pusat TNI AD Dharma Wiratama","Monumen Serangan Umum 1 Maret","Monumen Yogya Kembali"};
    // menampilkan list item dalam bentuk text dengan tipe data string dengan variable name
    String [] alamat={"Jl. Candi Sambisari, Purwomartani, Kalasan, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55571","Jalan Jendral Sudirman, Gowongan, Jetis, Gowongan, Jetis, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55233",
            "Dusun Cepit, Desa Bokoharjo, Prambanan, Sleman, Bokoharjo, Prambanan, Jogjakarta, Daerah Istimewa Yogyakarta 55572","Bendan, Tirtomartani, Kalasan, Tirtomartani, Kalasan, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55571",
            "Bokoharjo, Kec. Prambanan, Kabupaten Sleman, Daerah Istimewa Yogyakarta","Jl. Wonosari, Srimulyo, Piyungan, Bantul, Daerah Istimewa Yogyakarta 55792",
            "Jalan Rotowijayan Blok No. 1, Panembahan, Kraton, Kota Yogyakarta, Daerah Istimewa Yogyakarta","Jl. Raya Yogya - Solo, Suryatmajan, Danurejan, Daerah Istimewa Yogyakarta",
            "Jl. Wonosari, Baturetno, Banguntapan, Bantul, Daerah Istimewa Yogyakarta 55197","Jl. Margo Mulyo No.6, Ngupasan, Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122",
            "Jogotirto, Berbah, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55573","Jalan Pangurakan No. 6, Ngupasan, Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122",
            "Kompleks Landasan Udara Adisucipto, Jl. Kolonel Sugiono, Banguntapan, Yogyakarta, Bantul, Daerah Istimewa Yogyakarta 55282","Komplek Wisata Taman Sari, Taman, Patehan, Kraton, Patehan, Kraton, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55133",
            "Sendangtirto, Berbah, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55573","Jl. Laksda Adisucipto No.167, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281",
            "Jl. Veteran No.77, Warungboto, Umbulharjo, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55164","Jl. Jend. Sudirman No.75, Terban, Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55223",
            "Jalan Ahmad Yani, Ngupasan, Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122","Jl. Ringroad Utara No.15, RW.21, Sariharjo, Ngaglik, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55581"};
    LayoutInflater inflater;
    public RecyclerAdapterWisataBudaya(Context context) {
        this.context=context;
        inflater=LayoutInflater.from(context);
    }
    @Override
    public RecyclerViewHolderWisataBudaya onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=inflater.inflate(R.layout.list_wisata_budaya, parent, false);

        RecyclerViewHolderWisataBudaya viewHolder=new RecyclerViewHolderWisataBudaya(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolderWisataBudaya holder, int position) {

        holder.tv1.setText(name[position]);
        holder.tv1.setOnClickListener(clickListener);
        holder.tv2.setText(alamat[position]);
        holder.tv2.setOnClickListener(clickListener);
        holder.imageView.setOnClickListener(clickListener);
        holder.tv1.setTag(holder);
        holder.imageView.setTag(holder);

    }

    View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//member aksi saat cardview diklik berdasarkan posisi tertentu
            RecyclerViewHolderWisataBudaya vholder = (RecyclerViewHolderWisataBudaya) v.getTag();

            int position = vholder.getLayoutPosition();
            final Intent intent;
            switch (position) {
                case 0:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Candi+Sambisari/@-7.7624609,110.4450398,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5a39b544a4ab:0xd8f5c2d79ac124f3!8m2!3d-7.7624609!4d110.4472285?hl=id"));
                    break;
                case 1:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Tugu+Jogja/@-7.7828893,110.3648875,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a58308728df5d:0xe46a7c1c1d3b697a!8m2!3d-7.7828893!4d110.3670762?hl=id"));
                    break;
                case 2:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Candi+Banyunibo/@-7.7779751,110.4918831,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5a971d2ad689:0xb58c9748e70fd9e5!8m2!3d-7.7779751!4d110.4940718?hl=id"));
                    break;
                case 3:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Candi+Sari/@-7.7615132,110.4720844,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5af7df5430e3:0x6c8161c1354f3ac7!8m2!3d-7.7615132!4d110.4742731?hl=id"));
                    break;
                case 4:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Candi+Prambanan/@-7.7520206,110.4892787,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5ae3dbd859d1:0x19e7a03b25955a2d!8m2!3d-7.7520206!4d110.4914674?hl=id"));
                    break;
                case 5:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Situs+Payak/@-7.8334775,110.4553594,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a511d933a4cdd:0xa725587190712ffa!8m2!3d-7.8334775!4d110.4575481?hl=id"));
                    break;
                case 6:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Keraton+Yogyakarta/@-7.8052845,110.3620144,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5796db06c7ef:0x395271cf052b276c!8m2!3d-7.8052845!4d110.3642031?hl=id"));
                    break;
                case 7:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Candi+Kalasan/@-7.767284,110.4701618,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5a5f3dd682e7:0x744e3521829a27c1!8m2!3d-7.767284!4d110.4723505?hl=id"));
                    break;
                case 8:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Candi+Mantup/@-7.8245274,110.4060251,13z/data=!4m5!3m4!1s0x0:0xda4315fb3924b2df!8m2!3d-7.8156596!4d110.4176402?hl=id"));
                    break;
                case 9:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Museum+Benteng+Vredeburg/@-7.8002713,110.3641111,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5788c0b3eecf:0xb9611ce0232a9ff8!8m2!3d-7.8002713!4d110.3662998?hl=id"));
                    break;
                case 10:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Candi+Abang/@-7.8103728,110.4664973,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a508b5e9cb8e5:0xe4ec094fa5ffe7d9!8m2!3d-7.8103728!4d110.468686?hl=id"));
                    break;
                case 11:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Museum+Sonobudoyo/@-7.8019387,110.3622731,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a578f83070a4f:0x9d10431ac43ec5ee!8m2!3d-7.8019387!4d110.3644618?hl=id"));
                    break;
                case 12:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Museum+Pusat+TNI+AU+Dirgantara+Mandala/@-7.7899338,110.4134901,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5798c3e7637b:0x505f1eb0bb91bcf4!8m2!3d-7.7899338!4d110.4156788?hl=id"));
                    break;
                case 13:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Kampung+Wisata+Taman+Sari/@-7.8100073,110.3568078,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5793d0c2cf2b:0x276a21f8a01cbe13!8m2!3d-7.8100073!4d110.3589965?hl=id"));
                    break;
                case 14:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Candi+Klodangan/@-7.8127365,110.4240413,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a50b54cca2c49:0xb89bc074c995b810!8m2!3d-7.8127365!4d110.42623?hl=id"));
                    break;
                case 15:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Affandi+Museum/@-7.782713,110.3942083,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a59c49f681dbd:0x3e9d55bf26695d4a!8m2!3d-7.782713!4d110.396397?hl=id"));
                    break;
                case 16:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Situs+Peninggalan+Warungboto/@-7.8102755,110.3910437,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a576dd45f6243:0x47c4825dd428cf01!8m2!3d-7.8102755!4d110.3932324?hl=id"));
                    break;
                case 17:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Museum+Pusat+TNI+AD+Dharma+Wiratama/@-7.7828622,110.3731533,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5833a9864e7d:0x85ca2d673e7aceb4!8m2!3d-7.7828622!4d110.375342?hl=id"));
                    break;
                case 18:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Monumen+Serangan+Umum+1+Maret/@-7.8010089,110.363086,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5788b5ba6d3b:0x6722b1744aec3f53!8m2!3d-7.8010089!4d110.3652747?hl=id"));
                    break;
                case 19:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Monumen+Yogya+Kembali/@-7.7495904,110.3674181,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a58f99013c989:0x2a96db25b8ff4333!8m2!3d-7.7495904!4d110.3696068?hl=id"));
                    break;
                default:
                    intent = new Intent(context, MenuUtama.class);
                    break;
            }
            context.startActivity(intent);
        }
    };
    @Override
    public int getItemCount() {
        return name.length;
    }
}