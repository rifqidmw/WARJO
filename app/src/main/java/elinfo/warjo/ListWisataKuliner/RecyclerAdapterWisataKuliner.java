package elinfo.warjo.ListWisataKuliner;

/**
 * Created by Sei on 17/11/2017.
 */
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import elinfo.warjo.MenuUtama;
import elinfo.warjo.R;

public class RecyclerAdapterWisataKuliner extends  RecyclerView.Adapter<RecyclerViewHolderWisataKuliner> {


    //deklarasi variable context
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private final Context context;

    String [] name={"Gudeg Pawon","Nasi Goreng Beringharjo","Bong Kopitown","The House of Raminten",
            "Oseng-Oseng Mercon Bu Narti","Warung Makan Ayam Geprek Bu Rum 1","Soto Ayam Kampung Pak Dalbe",
            "Soto Sampah","Jejamuran","Gudeg Yu Djum Wijilan 167","Sate Klathak Pak Pong","Angkringan Lik Man",
            "Mie Ayam Grabyas"};
    // menampilkan list item dalam bentuk text dengan tipe data string dengan variable name
    String [] alamat = {"Jl. Janturan UH/IV No. 36, Warungboto, Umbulharjo, Warungboto, Umbulharjo, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55164","Jl. Mayor Suryotomo No.7, Ngupasan, Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122",
            "Jl. Sagan No.4, Terban, Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55223","Jalan Faridan Muridan Noto No.7, Kotabaru, Gondokusuman, Kotabaru, Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55224",
            "Jalan KH. Ahmad Dahlan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262","Jl. Wulung Lor, Papringan, Caturtunggal, Depok, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281",
            "Jl. Jenderal Sudirman, Terban, Gondokusuman, Terban, Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55224","Jl. Kranggan No.2, Cokrodiningratan, Jetis, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55233",
            "Jl. Pramuka No. 53, Pandowoharjo, Sleman, Pandowoharjo, Kec. Sleman, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55512","Jalan Wijilan No. 167, Kraton, Panembahan, Kraton, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55131",
            "Jalan Imogiri Timur Km. 10, Wonokromo, Pleret, Wonokromo, Pleret, Bantul, Daerah Istimewa Yogyakarta 55791","Pogung Lor No.263, Sinduadi, Mlati, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55284",
            "Gedongkiwo, Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55142"};

    LayoutInflater inflater;
    public RecyclerAdapterWisataKuliner(Context context) {
        this.context = context;
        inflater=LayoutInflater.from(context);
    }
    @Override
    public RecyclerViewHolderWisataKuliner onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=inflater.inflate(R.layout.list_wisata_kuliner, parent, false);

        RecyclerViewHolderWisataKuliner viewHolder=new RecyclerViewHolderWisataKuliner(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolderWisataKuliner holder, int position) {

        holder.tv1.setText(name[position]);
        holder.tv1.setOnClickListener(clickListener);
        holder.tv2.setText(alamat[position]);
        holder.tv2.setOnClickListener(clickListener);
        holder.imageView.setOnClickListener(clickListener);
        holder.tv1.setTag(holder);
        holder.imageView.setTag(holder);

    }

    View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//member aksi saat cardview diklik berdasarkan posisi tertentu
            RecyclerViewHolderWisataKuliner vholder = (RecyclerViewHolderWisataKuliner) v.getTag();

            int position = vholder.getLayoutPosition();
            final Intent intent;
            switch (position) {
                case 0:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Gudeg+Pawon/@-7.805718,110.3879509,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a57700347cb51:0xed0b575329f65c3d!8m2!3d-7.805718!4d110.3901396?hl=id"));
                    break;
                case 1:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Nasi+Goreng+Beringharjo/@-7.8007216,110.3667812,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5786798a6f8b:0x77224f44348be066!8m2!3d-7.8007216!4d110.3689699?hl=id"));
                    break;
                case 2:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Bong+Kopitown/@-7.7812838,110.3752072,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5834ab93b82b:0x6963eb30cf34c4d6!8m2!3d-7.7812838!4d110.3773959?hl=id"));
                    break;
                case 3:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/The+House+of+Raminten/@-7.7851487,110.3693064,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a583425adf533:0x587225db1e67a063!8m2!3d-7.7851487!4d110.3714951?hl=id"));
                    break;
                case 4:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Oseng-Oseng+Mercon+Bu+Narti/@-7.8010993,110.355595,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a577b7fe84623:0xbdcb4d1099f1050!8m2!3d-7.8010993!4d110.3577837?hl=id"));
                    break;
                case 5:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Warung+Makan+Ayam+Geprek+Bu+Rum+1/@-7.7757264,110.3931705,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a59c0c591ca7f:0x303b86dd382823fa!8m2!3d-7.7757264!4d110.3953592?hl=id"));
                    break;
                case 6:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Soto+Ayam+Kampung+Pak+Dalbe/@-7.7828792,110.3716404,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5833d2f38e09:0xfebd443ec866a3f4!8m2!3d-7.7828792!4d110.3738291?hl=id"));
                    break;
                case 7:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Soto+Sampah/@-7.7816265,110.3646682,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a58309d8d93a5:0xcf08db25c2460f04!8m2!3d-7.7816265!4d110.3668569?hl=id"));
                    break;
                case 8:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Jejamuran/@-7.7053727,110.3590232,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5f4b00000001:0xb991824588eb34e1!8m2!3d-7.7053727!4d110.3612119?hl=id"));
                    break;
                case 9:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Gudeg+Yu+Djum+Wijilan+167/@-7.8045771,110.3645781,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5785603eda9d:0x482e08cfedceed8f!8m2!3d-7.8045771!4d110.3667668?hl=id"));
                    break;
                case 10:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Sate+Klathak+Pak+Pong/@-7.8712464,110.385282,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a56854a9b3e95:0xdbe6f91fd89db72e!8m2!3d-7.8712464!4d110.3874707?hl=id"));
                    break;
                case 11:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Angkringan+Lik+Man/@-7.8712406,110.3174301,12z/data=!4m8!1m2!2m1!1sAngkringan+Lik+Man!3m4!1s0x2e7a58556f8e8ab5:0x14de278b8ef8674a!8m2!3d-7.755045!4d110.3764317?hl=id"));
                    break;
                case 12:
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Mie+Ayam+Grabyas/@-7.8032943,110.3421768,14z/data=!4m8!1m2!2m1!1sMie+Ayam+Grabyas!3m4!1s0x2e7a57eac3d3bf37:0x91f1195c486c6f9!8m2!3d-7.8156303!4d110.3533768?hl=id"));
                    break;
                default:
                    intent = new Intent(context, MenuUtama.class);
                    break;
            }
            context.startActivity(intent);
        }
    };
    @Override
    public int getItemCount() {
        return name.length;
    }
}