package elinfo.warjo.ListWisataKuliner;

/**
 * Created by Sei on 17/11/2017.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import elinfo.warjo.R;

public class RecyclerViewHolderWisataKuliner extends RecyclerView.ViewHolder {
    // ViewHolder akan mendeskripisikan item view yang ditempatkan di dalam RecyclerView.
    TextView tv1,tv2; //deklarasi textview
    ImageView imageView;

    private final Context context;
    public RecyclerViewHolderWisataKuliner(View itemView) {
        super(itemView);
        context = itemView.getContext();
        tv1= (TextView) itemView.findViewById(R.id.daftar_lokasi);
        //menampilkan text dari widget CardView pada id daftar_judul
        tv2= (TextView) itemView.findViewById(R.id.daftar_deskripsi);
        //menampilkan text deskripsi dari widget CardView pada id daftar_deskripsi
        imageView= (ImageView) itemView.findViewById(R.id.daftar_icon);
        //menampilkan gambar atau icon pada widget Cardview pada id daftar_icon
    }
}
