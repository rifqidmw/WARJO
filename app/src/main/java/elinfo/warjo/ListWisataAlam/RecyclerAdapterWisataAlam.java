package elinfo.warjo.ListWisataAlam;

/**
 * Created by Sei on 17/11/2017.
 */
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import elinfo.warjo.MenuUtama;
import elinfo.warjo.R;
import elinfo.warjo.WisataAlam;

public class RecyclerAdapterWisataAlam extends  RecyclerView.Adapter<RecyclerViewHolderWisataAlam> {


    //deklarasi variable context
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private final Context context;

    String [] name={"Puncak Becici","Pantai Indrayanti","Pantai Siung","Pantai Ngelambor",
            "Pantai Watu Lumbung","Pantai Sundak","Bukit Bintang","Blue Lagoon Jogja","Tlogo Putri Kaliurang",
            "Taman Tebing Breksi","Cave Tubing Kalisuci","Pantai Ngetun","Pantai Kayu Arum","Goa Jomblang",
            "Green Village Gedangsari","Gunung Api Purba Nglanggeran","Embung Batara Srinten","Air Terjun Luweng Sampang",
            "Pantai Sadeng","Pantai Ngrenehan","Embung Nglanggeran"};
    // menampilkan list item dalam bentuk text dengan tipe data string dengan variable name
    String [] alamat = {"Gunungcilik RT. 07 / RW. 02, Muntuk, Dlingo, Muntuk, Dlingo, Bantul, Daerah Istimewa Yogyakarta 55783","Tepus, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta",
            "Purwodadi, Tepus, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55881","Purwodadi, Tepus, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta",
            "Balong, Girisubo, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55883","Tepus, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55881",
            "Patuk, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55792","Widodomartani, Ngemplak, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55584",
            "Jl. Tlogo Putri, Hargobinangun, Pakem, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55582",
            "Sambirejo, Prambanan, Sambirejo, Prambanan, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55572",
            "Jetisa Wetan, Pacarejo, Semanu, Pacarejo, Semanu, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55893",
            "Sureng, Purwodadi, Tepus, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55881",
            "Kanigoro, Tanjungsari, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55871",
            "Pacarejo, Semanu, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55893",
            "Mertelu, Gedang Sari, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55863",
            "Nglanggeran, Patuk, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55862",
            "Pilangrejo, Nglipar, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55852",
            "Jl. Juminahan, Sampang, Gedang Sari, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55863",
            "Songbanyu, Girisubo, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55883",
            "Wonosari, Kanigoro, Sapto Sari, Gunung Kidul Regency, Special Region of Yogyakarta 55871",
            "Nglanggeran, Patuk, Nglanggeran, Patuk, Kabupaten Gunung Kidul, Daerah Istimewa Yogyakarta 55862"};

    LayoutInflater inflater;
    public RecyclerAdapterWisataAlam(Context context) {
        this.context = context;
        inflater=LayoutInflater.from(context);
    }
    @Override
    public RecyclerViewHolderWisataAlam onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=inflater.inflate(R.layout.list_wisata_alam, parent, false);

        RecyclerViewHolderWisataAlam viewHolder=new RecyclerViewHolderWisataAlam(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolderWisataAlam holder, int position) {

        holder.tv1.setText(name[position]);
        holder.tv1.setOnClickListener(clickListener);
        holder.tv2.setText(alamat[position]);
        holder.tv2.setOnClickListener(clickListener);
        holder.imageView.setOnClickListener(clickListener);
        holder.tv1.setTag(holder);
        holder.imageView.setTag(holder);

    }

    View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//member aksi saat cardview diklik berdasarkan posisi tertentu
            RecyclerViewHolderWisataAlam vholder = (RecyclerViewHolderWisataAlam) v.getTag();

            int position = vholder.getLayoutPosition();
            final Intent intent;
            switch (position) {
                case 0:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Puncak+Pinus+Becici/@-7.901611,110.4353313,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a531aaaaaaaab:0x6c28149b91ea59f4!8m2!3d-7.901611!4d110.43752?hl=id"));
                    break;
                case 1:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Pantai+Indrayanti/@-8.1507886,110.6103773,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7bba2bebcb0c7f:0x854daae21d9d9724!8m2!3d-8.1506589!4d110.6126311?hl=id"));
                    break;
                case 2:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Pantai+Siung/@-8.1822492,110.6810083,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7bb8c8a5ee1c27:0x567f73117ae7ce4a!8m2!3d-8.1818233!4d110.6832394?hl=id"));
                    break;
                case 3:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Pantai+Nglambor/@-8.1827028,110.6770512,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7bb3ffffffffff:0x16a5f30f0c719ffb!8m2!3d-8.1827028!4d110.6792399?hl=id"));
                    break;
                case 4:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Pantai+Watu+Lumbung/@-8.1836124,110.6964826,16z/data=!4m13!1m7!3m6!1s0x2e7bb8ba97a2f50f:0x31dd2f88e26588a5!2sJl.+Pantai+Watu+Lumbung,+Balong,+Girisubo,+Kabupaten+Gunung+Kidul,+Daerah+Istimewa+Yogyakarta+55883!3b1!8m2!3d-8.1818906!4d110.6996676!3m4!1s0x0:0x15c44b48d96babc1!8m2!3d-8.1860215!4d110.7002793?hl=id"));
                    break;
                case 5:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Pantai+Sundak/@-8.1471482,110.6056589,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7bba2e55492ca1:0xcaee252e6bf05e80!8m2!3d-8.147072!4d110.6079043?hl=id"));
                    break;
                case 6:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Bukit+Bintang+%2F+Hargodumilah/@-7.8456572,110.4780534,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a51a418acd7d3:0xfb055b6604992f0a!8m2!3d-7.8456572!4d110.4802421?hl=id"));
                    break;
                case 7:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Blue+Lagoon+Jogja/@-7.7044358,110.4480713,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5c75eba9da15:0xd4d6063b3b163462!8m2!3d-7.7044358!4d110.45026?hl=id"));
                    break;
                case 8:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Tlogo+Putri+Kaliurang/@-7.5942753,110.4283509,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a672c47087c37:0x38ff2c166bf45c37!8m2!3d-7.5942753!4d110.4305396?hl=id"));
                    break;
                case 9:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Taman+Tebing+Breksi/@-7.7823218,110.5025821,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a5aa46eac45ff:0xf4d2d3dc97e5ce3f!8m2!3d-7.7823218!4d110.5047708?hl=id"));
                    break;
                case 10:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Kalisuci+Tubing/@-8.0099538,110.6368159,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7bb415162810d3:0x3e219ef349b8ce93!8m2!3d-8.0099538!4d110.6390046?hl=id"));
                    break;
                case 11:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Pantai+Ngetun/@-8.1703641,110.6504559,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7bb9177e280a29:0x108da391bd9ca0a0!8m2!3d-8.1703641!4d110.6526446?hl=id"));
                    break;
                case 12:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Pantai+Kayu+Arum/@-8.1295496,110.5320458,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7bbabdf03c5607:0x50169f0ec086d6a0!8m2!3d-8.1295496!4d110.5342345?hl=id"));
                    break;
                case 13:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Goa+jomblang/@-8.02866,110.6362083,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7bb41fce34d717:0xf9ffcb6f9fbd5cce!8m2!3d-8.02866!4d110.638397?hl=id"));
                    break;
                case 14:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Green+Village+Gedangsari/@-7.8130941,110.613382,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a4f32bf83986b:0x89cfc63831ddf2a3!8m2!3d-7.8130941!4d110.6155707?hl=id"));
                    break;
                case 15:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Gunung+Api+Purba+Nglanggeran/@-7.8425103,110.5368011,15z/data=!4m8!1m2!2m1!1sGunung+Api+Purba+Nglanggeran!3m4!1s0x2e7a4e46c945ab9b:0x4ad51e14b7baaf61!8m2!3d-7.8430348!4d110.5495358?hl=id"));
                    break;
                case 16:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Embung+Batara+Sriten/@-7.8321113,110.6308283,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a4be555555555:0x404cf863f23e1aaa!8m2!3d-7.8321113!4d110.633017?hl=id"));
                    break;
                case 17:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Luweng+Sampang/@-7.8098267,110.5700219,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a57876d285fc7:0xcca955bfd64b840d!8m2!3d-7.8098267!4d110.5722106?hl=id"));
                    break;
                case 18:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Pantai+Sadeng/@-8.1888792,110.7968395,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7bc153abfcfe7f:0xdb72811db9bee849!8m2!3d-8.1888792!4d110.7990282?hl=id"));
                    break;
                case 19:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Pantai+Ngrenehan/@-8.1211888,110.5120256,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7ba5568b9ebe5b:0x4ae27d9d8fda79d4!8m2!3d-8.1211475!4d110.5142337?hl=id"));
                    break;
                case 20:
                    intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("https://www.google.co.id/maps/place/Embung+Nglanggeran/@-7.8469809,110.5447048,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a4e40416ef711:0x5e15a88116b3577e!8m2!3d-7.8469809!4d110.5468935?hl=id"));
                    break;
                default:
                    intent = new Intent(context, MenuUtama.class);
                    break;
            }
            context.startActivity(intent);
        }
    };
    @Override
    public int getItemCount() {
        return name.length;
    }
}