package elinfo.warjo;

import android.content.Intent;
import android.hardware.SensorManager;
import android.location.LocationListener;
import android.net.Uri;
import android.widget.Toast;

import elinfo.warjo.ARview.AbstractArchitectCamActivity;
import elinfo.warjo.ARview.ArchitectViewHolderInterface;
import elinfo.warjo.ARview.LocationProvider;
import elinfo.warjo.ARview.PoiDetailActivity;
import elinfo.warjo.ARview.WikitudeSDKConstants;
import com.wikitude.architect.ArchitectView;
import com.wikitude.architect.StartupConfiguration;

public class WisataAlam extends AbstractArchitectCamActivity {

    private long lastCalibrationToastShownTimeMillis = System.currentTimeMillis();

    @Override
    protected StartupConfiguration.CameraPosition getCameraPosition() {
        return StartupConfiguration.CameraPosition.DEFAULT;
    }

    @Override
    protected boolean hasGeo() {
        return true;
    }

    @Override
    protected boolean hasIR() {
        return false;
    }

    @Override
    public String getActivityTitle() {
        return "";
    }

    @Override
    public String getARchitectWorldPath() {
        return "warjo/wisataalam/index.html";
    }

    @Override
    public ArchitectView.ArchitectUrlListener getUrlListener() {
        return new ArchitectView.ArchitectUrlListener() {
            public boolean urlWasInvoked(String paramAnonymousString) {
                Uri localUri = Uri.parse(paramAnonymousString);
                if ("markerselected".equalsIgnoreCase(localUri.getHost())) {
                    Intent localIntent = new Intent(WisataAlam.this, PoiDetailActivity.class);
                    localIntent.putExtra("id", String.valueOf(localUri.getQueryParameter("id")));
                    localIntent.putExtra("title", String.valueOf(localUri.getQueryParameter("title")));
                    localIntent.putExtra("alamat", String.valueOf(localUri.getQueryParameter("alamat")));
                    localIntent.putExtra("description", String.valueOf(localUri.getQueryParameter("description")));
                    localIntent.putExtra("latitude", Double.valueOf(localUri.getQueryParameter("latitude")));
                    localIntent.putExtra("longitude", Double.valueOf(localUri.getQueryParameter("longitude")));
                    localIntent.putExtra("img", String.valueOf(localUri.getQueryParameter("img")));
                    localIntent.putExtra("img1", String.valueOf(localUri.getQueryParameter("img1")));
                    localIntent.putExtra("img2", String.valueOf(localUri.getQueryParameter("img2")));
                    localIntent.putExtra("img3", String.valueOf(localUri.getQueryParameter("img3")));
                    localIntent.putExtra("img4", String.valueOf(localUri.getQueryParameter("img4")));
                    localIntent.putExtra("img5", String.valueOf(localUri.getQueryParameter("img5")));
                    WisataAlam.this.startActivity(localIntent);
                }
                return true;
            }
        };
    }



    @Override
    public int getContentViewId() {
        return R.layout.activity_wisata_alam;
    }

    @Override
    public String getWikitudeSDKLicenseKey() {
        return WikitudeSDKConstants.WIKITUDE_SDK_KEY;
    }

    @Override
    public int getArchitectViewId() {
        return R.id.architectView;
    }

    @Override
    public ILocationProvider getLocationProvider(LocationListener locationListener) {
        return new LocationProvider(this, locationListener);
    }






    @Override
    public ArchitectView.SensorAccuracyChangeListener getSensorAccuracyListener() {
        return new ArchitectView.SensorAccuracyChangeListener() {
            @Override
            public void onCompassAccuracyChanged( int accuracy ) {
				/* UNRELIABLE = 0, LOW = 1, MEDIUM = 2, HIGH = 3 */
                if ( accuracy < SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM && WisataAlam.this != null && !WisataAlam.this.isFinishing() && System.currentTimeMillis() - WisataAlam.this.lastCalibrationToastShownTimeMillis > 5 * 1000) {
                    Toast.makeText(WisataAlam.this, R.string.compass_accuracy_low, Toast.LENGTH_LONG).show();
                    WisataAlam.this.lastCalibrationToastShownTimeMillis = System.currentTimeMillis();
                }
            }
        };
    }

    @Override
    public float getInitialCullingDistanceMeters() {
        return ArchitectViewHolderInterface.CULLING_DISTANCE_DEFAULT_METERS;

    }

}
