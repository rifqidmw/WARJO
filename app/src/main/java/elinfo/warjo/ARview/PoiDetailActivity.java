package elinfo.warjo.ARview;

/**
 * Created by Sei on 14/11/2017.
 */

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import elinfo.warjo.R;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.gc.materialdesign.views.ButtonRectangle;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by root on 13/03/16.
 */
public class PoiDetailActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRAS_KEY_POI_ADDR = "address";
    public static final String EXTRAS_KEY_POI_DESCR = "description";
    public static final String EXTRAS_KEY_POI_ID = "id";
    public static final String EXTRAS_KEY_POI_IMG = "img";
    public static final String EXTRAS_KEY_POI_IMG1 = "img1";
    public static final String EXTRAS_KEY_POI_IMG2 = "img2";
    public static final String EXTRAS_KEY_POI_IMG3 = "img3";
    public static final String EXTRAS_KEY_POI_IMG4 = "img4";
    public static final String EXTRAS_KEY_POI_IMG5 = "img5";
    public static final String EXTRAS_KEY_POI_LAT = "latitude";
    public static final String EXTRAS_KEY_POI_LNG = "longitude";
    public static final String EXTRAS_KEY_POI_PHONE = "phone";
    public static final String EXTRAS_KEY_POI_TITLE = "title";
    private String alamat;
    private ButtonRectangle btnGetTransport;
    private Button btnGetDirection;
    private String deskripsi;
    private String img;
    private String img1;
    private String img2;
    private String img3;
    private String img4;
    private String img5;
    private SliderLayout imgSlider;
    private LatLng lokasiTujuan;
    private Toolbar mToolbar;
    private GoogleMap map;
    private String nama;
    private String telepon;
    private TextView tvAlamat;
    private TextView tvDeskripsi;
    private TextView tvNama;
    private TextView tvTelepon;

    private WebView view;



    private void initialize() {
        //this.tvNama = ((TextView) findViewById(R.id.poi_title));
        //this.tvAlamat = ((TextView) findViewById(R.id.poi_address));
        //this.tvTelepon = ((TextView) findViewById(R.id.poi_phone));
        //this.tvDeskripsi = ((TextView) findViewById(R.id.poi_description));
        this.btnGetDirection = ((Button) findViewById(R.id.btnDirection));

        this.btnGetDirection.setOnClickListener(this);
        //this.imgSlider = ((SliderLayout) findViewById(R.id.slider));
        HashMap localHashMap = new HashMap();
        localHashMap.put("img", getIntent().getExtras().getString("img"));
        localHashMap.put("img1", getIntent().getExtras().getString("img1"));
        localHashMap.put("img2", getIntent().getExtras().getString("img2"));
        localHashMap.put("img3", getIntent().getExtras().getString("img3"));
        localHashMap.put("img4", getIntent().getExtras().getString("img4"));
        localHashMap.put("img5", getIntent().getExtras().getString("img5"));
        Iterator localIterator = localHashMap.keySet().iterator();
        while (localIterator.hasNext()) {
            String str = (String) localIterator.next();
            DefaultSliderView localDefaultSliderView = new DefaultSliderView(this);
            localDefaultSliderView.image((String) localHashMap.get(str)).setScaleType(BaseSliderView.ScaleType.CenterCrop);
            localDefaultSliderView.bundle(new Bundle());
            localDefaultSliderView.getBundle().putString("extra", str);
//            this.imgSlider.addSlider(localDefaultSliderView);
        }
        // this.imgSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        //this.imgSlider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));
        //this.imgSlider.setDuration(5000L);
    }

    private void setTeksView() {
//        this.tvNama.setText(this.nama);
        this.tvAlamat.setText(this.alamat);
        this.tvTelepon.setText(this.telepon);
        this.tvDeskripsi.setText(this.deskripsi);
    }

    private void setupMap() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        this.map.setMyLocationEnabled(true);
        this.map.addMarker(new MarkerOptions().position(new LatLng(this.lokasiTujuan.latitude, this.lokasiTujuan.longitude)).title(this.nama));
        this.map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.lokasiTujuan.latitude, this.lokasiTujuan.longitude), 10.0F));
        this.map.getUiSettings().setZoomControlsEnabled(true);
        this.map.getUiSettings().setZoomGesturesEnabled(true);
    }
    /**
     private void setupMapIfNeeded()
     {
     if (this.map == null)
     {
     this.map = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.maps)).getMap();
     if (this.map != null) {
     setupMap();
     }
     }
     }
     */
    private void setupToolbar()
    {
//        setSupportActionBar(this.mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.mToolbar.setTitleTextColor(getResources().getColor(R.color.colorToolbarText));
        this.mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View paramAnonymousView) {
                PoiDetailActivity.this.onBackPressed();
            }
        });
    }
    @Override
    public void onClick(View v) {
        if (v == this.btnGetDirection) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?daddr=" + this.lokasiTujuan.latitude + "," + this.lokasiTujuan.longitude)));
        }
        if (v == this.btnGetTransport){

        }
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        setContentView(R.layout.activity_poidetail);
        initialize();
        Bundle localBundle = getIntent().getExtras();
        if (localBundle != null)
        {
            this.nama = localBundle.getString("title");
            this.alamat = localBundle.getString("alamat");
            this.telepon = localBundle.getString("phone");
            this.deskripsi = localBundle.getString("description");
            this.lokasiTujuan = new LatLng(localBundle.getDouble("latitude"), localBundle.getDouble("longitude"));
        }
//        setTeksView();
        //setupMapIfNeeded();
        this.mToolbar = ((Toolbar)findViewById(R.id.toolbar));
//        setupToolbar();

        view = (WebView) this.findViewById(R.id.webView);
        view.getSettings().setJavaScriptEnabled(true);
        view.setWebViewClient(new MyBrowser());
        view.loadUrl("http://"+deskripsi);
        view.getSettings().setBuiltInZoomControls(true);
        view.getSettings().setSupportZoom(true);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //ketika disentuh tombol back
        if ((keyCode == KeyEvent.KEYCODE_BACK) && view.canGoBack()) {
            view.goBack(); //method goback() dieksekusi untuk kembali pada halaman sebelumnya
            return true;
        }
        // Jika tidak ada history (Halaman yang sebelumnya dibuka)
        // maka akan keluar dari activity
        return super.onKeyDown(keyCode, event);
    }

    protected void onStop()
    {
//        this.imgSlider.stopAutoCycle();
        super.onStop();
    }
}